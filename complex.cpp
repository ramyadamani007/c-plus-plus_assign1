/*****************
	Include
*****************/

#include<iostream>

using namespace std;

/**** Class Declaration ****/

class complex
{
	private:
		float p,q;
		
	public :
  		void set(float r,float img)
  		{
  			p = r;
  			q =img;
  		}
  		
		
		complex sum (complex z)
		{
			complex y;
			y.p = p + z.p;
			y.q = q + z.q;
			return y;
		}


		void disp()
		{
			cout<<p<<" + j"<<q<<endl;
		}
};



int main ()
{
	complex y1,y2,y3;
	y1.set (5.1,2.3);
	y2.set (4.6,3.7);
	y3 = y1.sum (y2);
	cout<<"\nThe first complex number is ";y1.disp();
	cout<<"\nThe second complex number is ";y2.disp();
	cout<<"\nThe addition of both complex number is ";y3.disp();
	return 0;
}











